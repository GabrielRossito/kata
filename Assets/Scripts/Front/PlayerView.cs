﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerView : MonoBehaviour
{
    [SerializeField]
    private Text Name, Life;
    [SerializeField]
    private PlayerHand _playerHand;
    
    private Player _player;
    public Player Player
    {
        get { return _player; }
        set {
            _player = value;
            Name.text = _player.Name;
            Life.text = _player.LifePoints.ToString("00");
        }
    }

    public void InitializePlayerHand()
    {
        _playerHand.CreateHand(Player.Hand);
    }
}
