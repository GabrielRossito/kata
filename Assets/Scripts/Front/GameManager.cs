﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private PlayerView _playerOneView, _playerTwoView;

    private Player _playerOne;
    private Player _playerTwo;

    void Start ()
    {
        _playerOne = new Player("Josieta");
        _playerOneView.Player = _playerOne;
        _playerOneView.InitializePlayerHand();

        _playerTwo = new Player("Joaquina");
        _playerTwoView.Player = _playerTwo;
        _playerTwoView.InitializePlayerHand();
	}
}
