﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class PlayerHand : MonoBehaviour
{
    public class CardEvent : UnityEvent<Card> { };
    public CardEvent onCardPlayed = new CardEvent();

    [SerializeField]
    private GameObject _cardPrefab;
    [SerializeField]
    private int _amountPlayableCards = 4;
    [SerializeField]
    private RectTransform _handCardsHolder;

    private int _currentAmountSelectedCards = 0;
    private List<GameObject> _cardsObjs = new List<GameObject>();

    public void CreateHand(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            GameObject card = Instantiate(_cardPrefab);
            RectTransform rect = card.GetComponent<RectTransform>();
            rect.SetParent(_handCardsHolder);
            rect.localPosition = Vector3.zero;
            rect.anchoredPosition = Vector2.zero;
            rect.localScale = Vector3.one;
            rect.localRotation = Quaternion.identity;

            card.GetComponent<CardView>().Card = cards[i];
            card.GetComponent<CardView>().onSelect.AddListener(OnSelectedCard);

            _cardsObjs.Add(card);
        }
    }

    private void OnSelectedCard(Card card)
    {


        _currentAmountSelectedCards++;
        if (_currentAmountSelectedCards > _amountPlayableCards - 1)
        {
            for (int i = 0; i < _cardsObjs.Count; i++)
            {
                if (_cardsObjs[i] != null)
                    Destroy(_cardsObjs[i]);
            }
            _cardsObjs.Clear();
        }
        onCardPlayed.Invoke(card);
    }
}
