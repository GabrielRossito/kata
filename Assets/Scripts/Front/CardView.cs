﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CardView : MonoBehaviour
{
    public class CardEvent : UnityEvent<Card> { };
    public CardEvent onSelect = new CardEvent();

    [SerializeField]
    private Text Type, Power;

    private Card _card;
    public Card Card
    {
        get { return _card; }
        set
        {
            _card = value;
            Power.text = _card.Power.ToString("000");
            Type.text = _card.Type.ToString();
        }
    }
    public Button Btn
    {
        get { return GetComponent<Button>(); }        
    }

    void Start()
    {
        Btn.onClick.AddListener(Select);
    }

    public void Select()
    {
        Debug.Log("Type: " + Card.Type + " Power: " + Card.Power);
        Destroy(gameObject);

        onSelect.Invoke(Card);
    }    
}
