﻿using System;

[Serializable]
public class Dice
{
    public int Faces = 6;
    public Dice() { }
    public Dice(int faces)
    {
        Faces = faces;
    }

    public int Roll()
    {
        return UnityEngine.Random.Range(1, Faces+1);
    }
}
