﻿using System.Collections.Generic;

public class PlayerBoard
{
    public List<Card> BoardCards { get; private set; }

    public PlayerBoard()
    {
        BoardCards = new List<Card>();
    }
}
