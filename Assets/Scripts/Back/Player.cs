﻿using System.Collections.Generic;

public class Player
{
    public string Name { get; private set; }
    public float LifePoints { get; private set; }

    private List<Card> _hand;
    public List<Card> Hand { get { return _hand; } }

    private Deck Deck;

    private PlayerBoard _playerBoard;
    public PlayerBoard PlayerBoard { get { return _playerBoard; } }

    public Player(string name, float life = 20, int handSize = 8)
    {
        Name = name;
        LifePoints = life;
        Deck = new Deck();
        Deck.Shuffle(10);
        _hand = Deck.DrawHand(handSize);
    }

    public override string ToString()
    {
        string cards = "\n";
        foreach (Card card in _hand)
            cards += card.Power + " " + card.Type + "\n";

        return string.Format("{0} Life: {1}, HandSize: {2} Cards: {3}", Name, LifePoints, Hand.Count, cards);
    }
}
