﻿using System;

[Serializable]
public class Card
{
    public Card(GoblinType type, int power)
    {
        Type = type;
        Power = power;
    }

    public GoblinType Type;
    public int Power;
}
