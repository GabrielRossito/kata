﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class Deck
{
    [SerializeField]
    private Stack<Card> _cards;

    public Deck()
    {
        _cards = new Stack<Card>();
        for (int i = 1; i <= 8; i++)
            _cards.Push(new Card(GoblinType.Artillery, i));
        for (int i = 1; i <= 8; i++)
            _cards.Push(new Card(GoblinType.Infantry, i));
    }

    public Card Draw()
    {
        return _cards.Pop();
    }

    public void Shuffle(int qnt = 1)
    {
        while (qnt >= 0)
        {
            Stack<Card> someDeck1 = new Stack<Card>();
            Stack<Card> someDeck2 = new Stack<Card>();
            Stack<Card> someDeck3 = new Stack<Card>();

            int rand, size = _cards.Count;
            for (int i = 0; i < size; i++)
            {
                rand = UnityEngine.Random.Range(1, 4);
                switch (rand)
                {
                    case 1:
                        someDeck1.Push(_cards.Pop());
                        break;
                    case 2:
                        someDeck2.Push(_cards.Pop());
                        break;
                    case 3:
                        someDeck3.Push(_cards.Pop());
                        break;
                }
            }
            for (int i = 0; i < size; i++)
            {
                if (someDeck1.Count > 0)
                    _cards.Push(someDeck1.Pop());
                if (someDeck2.Count > 0)
                    _cards.Push(someDeck2.Pop());
                if (someDeck3.Count > 0)
                    _cards.Push(someDeck3.Pop());
            }
            qnt--;
        }
    }

    public List<Card> DrawHand(int size = 8)
    {
        List<Card> cards = new List<Card>();
        for (int i = 0; i < size; i++)
            cards.Add(Draw());

        return cards;
    }
}
